#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from multiprocessing import Process

from main.configuration import Configuration
from main.server import Server
from main.payload import Payload


class Entrypoint:
    def __init__(self):
        self.config = Configuration()

    def main(self):
        try:
            processes = [
                Process(target=Server().spawn),
                Process(target=Payload().spawn)
            ]
            for p in processes:
                p.start()
            for p in processes:
                p.join()
        except Exception as e:
            logging.error(f'Entrypoint: {str(e)}')
        pass


def entrypoint():
    Entrypoint().main()


if __name__ == '__main__':
    entrypoint()
