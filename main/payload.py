#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generate traffic to main.server.Server with random payload and frequency
"""

import logging
import requests
from random import randint
from time import sleep
try:
    import simplejson as json
except ImportError:
    import json


from main.configuration import Configuration


class Payload:
    def __init__(self):
        self.config = Configuration().payload

    def spawn(self):
        while True:
            # random delay
            delay = randint(
                self.config.min_delay,
                self.config.max_delay
            ) / 1000
            sleep(delay)
            # random request method
            if randint(0, 100) >= 70:
                method = requests.get
            else:
                method = requests.post
            # generate payload
            min_len = 128
            max_len = 1024
            payload_len = randint(min_len, max_len)
            payload = ''
            alphabet = 'abcdefghijklmnoprstuvwxyz'
            for i in range(payload_len):
                if randint(0, 100) >= 50:
                    payload += alphabet[randint(0, len(alphabet) - 1)].lower()
                else:
                    payload += alphabet[randint(0, len(alphabet) - 1)].upper()
            # proceed request
            try:
                if randint(0, 100) <= 5:
                    r = method(
                        url=f'http://127.0.0.1:{self.config.port}',
                        headers={'Status-Code': '400'}
                    )
                else:
                    r = method(
                        url=f'http://localhost:{self.config.port}',
                        data=payload
                    )
                j = r.json()
                logging.debug(f'Payload: response:\n{json.dumps(j, sort_keys=True, indent=2)}')
            except Exception as e:
                logging.error(f'Payload: {str(e)}')
