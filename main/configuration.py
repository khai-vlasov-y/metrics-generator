#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Read about environment variables in README.md

import logging

from os import getenv
from socket import gethostname


class Configuration(object):
    singleton_object = None

    @staticmethod
    def configure_logging():
        """
        Configure logging depending on server settings.
        :return:
        """
        logging_format = '%(asctime)s [%(levelname)s] Line: %(lineno)d | %(message)s'
        if getenv('DEBUG', 'false').lower() == 'true':
            logging_level = logging.DEBUG
        else:
            logging_level = logging.INFO
        logging.basicConfig(format=logging_format, level=logging_level)

    def __new__(cls, *args, **kwargs):
        if not Configuration.singleton_object:
            Configuration.configure_logging()
            logging.debug('Config: creating new object')
            Configuration.singleton_object = super().__new__(cls, *args, **kwargs)
        return Configuration.singleton_object

    class GeneralConfiguration:
        def __init__(self, debug):
            self.debug = debug

    class ServerConfiguration:
        def __init__(self, port):
            self.port = port

    class GraphiteConfiguration:
        def __init__(self, server, port, prefix, system_name):
            self.server = server
            self.port = port
            self.prefix = f'{prefix}.{system_name}'

    class PayloadConfiguration():
        def __init__(self, max_delay, min_delay, port):
            self.max_delay = max_delay
            self.min_delay = min_delay
            self.port = port    # duplicate of ServerConfiguration.port

    def __init__(self):
        try:
            if self.initialized:
                return
        except AttributeError:
            pass
        logging.debug('Config: init() called')
        try:
            self.config = {}
            self.general = Configuration.GeneralConfiguration(
                debug=getenv('DEBUG', 'false').lower() == 'true'
            )
            self.server = Configuration.ServerConfiguration(
                port=int(getenv('PORT', 8080))
            )
            self.graphite = Configuration.GraphiteConfiguration(
                server=getenv('GRAPHITE_SERVER', 'localhost'),
                port=getenv('GRAPHITE_PORT', 8125),
                prefix=getenv('METRICS_PREFIX', 'cloud.computing'),
                system_name=getenv('SYSTEM_NAME', gethostname())
            )
            max_delay = int(getenv('PAYLOAD_MAX_DELAY', 100))
            min_delay = int(getenv('PAYLOAD_MIN_DELAY', 50))
            max_delay = max(max_delay, min_delay)
            min_delay = min(max_delay, min_delay)
            self.payload = Configuration.PayloadConfiguration(
                max_delay=max_delay,
                min_delay=min_delay,
                port=self.server.port
            )
            logging.debug('Config: initialization done')
            self.initialized = True
        except AttributeError as e:
            logging.error(f'Config: initialization error: {str(e)}')
            exit(1)
