#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Server listen HTTP port, answer with timestamp back to client and sends metrics to Graphite about received request.
"""

import logging
from http import server
from time import strftime
from statsd import StatsClient
try:
    import simplejson as json
except ImportError:
    import json

from main.configuration import Configuration


class Server:
    statsd = StatsClient(
        host=Configuration().graphite.server,
        port=Configuration().graphite.port,
        prefix=Configuration().graphite.prefix
    )

    def __init__(self):
        self.config = Configuration().server

    class Handler(server.SimpleHTTPRequestHandler):
        def do_GET(self):
            Server.statsd.incr(stat='request_type.get')
            self.serve_new_request()

        def do_POST(self):
            Server.statsd.incr(stat='request_type.post')
            self.serve_new_request()

        def serve_new_request(self):
            """
            Serves incoming request.
            :return:
            """
            logging.debug(f'Server: request from {self.client_address}')
            # answer on request
            try:
                response = dict(
                    timestamp=strftime('%Y-%m-%d %H:%M:%S')
                )
                if self.headers.get('Content-Length', None):
                    payload = self.rfile.read(int(self.headers['Content-Length']))
                    response['payload'] = dict(
                        data=str(payload),
                        size=len(payload)
                    )
                else:
                    # if no payload
                    self.send_response(400)
                    self.end_headers()
                    self.wfile.write(bytes('No Content-Length header.', "utf8"))
                    Server.statsd.incr(stat='status_code.400')
                    return
                if self.headers.get('Status-Code', None):
                    code = int(self.headers.get('Status-Code', None))
                    self.send_response(code)
                    self.end_headers()
                    Server.statsd.incr(stat=f'status_code.{code}')
                # Send response status code
                self.send_response(200)
                # Send headers
                self.send_header('Content-type', 'javascript/json')
                self.end_headers()
                # send metrics
                Server.statsd.incr(stat='status_code.200')
                Server.statsd.gauge(stat='payload.size', value=response['payload']['size'])
                # send response
                response = json.dumps(response, sort_keys=True, indent=2)
                logging.debug(f'Server: response json:\n{response}')
                self.wfile.write(bytes(response, "utf8"))
            except Exception as e:
                logging.error(f'Server: error: {str(e)}')
                try:
                    self.send_response(500)
                    self.end_headers()
                except:
                    pass

    def spawn(self):
        httpd = server.HTTPServer(
            server_address=('0.0.0.0', self.config.port),
            RequestHandlerClass=Server.Handler
        )
        httpd.serve_forever()
