# Brief

Generates metrics and upload them to graphite.

# Environment variables
| Name | Default | Description | Required | Example |
| --- | --- | --- | --- | --- |
| DEBUG | false | Add more verbosity - debug mode | no | true |
| PORT | 8080 | HTTP web server port | no | 80 |
| GRAPHITE_SERVER | localhost | Graphite server hostname or IP | no | 192.168.1.1 |
| GRAPHITE_PORT | 8125 | Statsd port | no | 8125 |
| METRICS_PREFIX | cloud.computing | Prefix for all metrics sent to Graphite | no | You should satisfy `^[a-z_]+(\.[a-z_]+)*$` |
| SYSTEM_NAME | current hostname | System name of this instance - will be as last dir in full metrics path | no | `[a-zA-Z]+` |
| PAYLOAD_MAX_DELAY | 100 | Max delay in ms between requests to server | no | 500 |